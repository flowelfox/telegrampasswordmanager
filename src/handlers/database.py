import base64
import binascii
import enum
import hashlib
import logging
import os

import formencode
from emoji import emojize
from formencode import validators
from pykeepass import PyKeePass
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ConversationHandler, MessageHandler, filters, CallbackQueryHandler, CommandHandler

from src.handlers.add_edit import AddEditMenu
from src.handlers.base import BaseMenu
from src.handlers.entry import EntryMenu
from src.handlers.group import GroupMenu
from src.lib.crypt import AESCipher
from src.models import DBSession, PasswordDatabase, TelegramPasswordDatabase, TelegramPasswordGroup, DatabaseEntry, DatabaseGroup
from src.settings import SECRET, ITEMS_PER_PAGE, MEDIA_FOLDER, RESOURCES_FOLDER, KDBX_SAMPLE_NAME, KDBX_SAMPLE_PASSWORD

logger = logging.getLogger(__name__)


class DatabaseStates(enum.Enum):
    ACTION = 1
    PASSWORD = 2
    DELETE_CONFIRMATION = 3
    CREATE_WHAT = 4
    INPUT_VALUES_ENTRY = 5
    EDIT_DATABASE_APPLY = 6
    IMPORT_GET_FILE = 8
    IMPORT_VARIANT_CHOOSE = 9
    IMPORT_PASSWORD = 10
    IMPORT_FILE_KEY = 11
    IMPORT_REAL_PASSWORD = 12
    DOWNLOAD = 13
    DOWNLOAD_DONE = 14


class DatabaseMenu(BaseMenu):

    def entry(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']

        data = int(update.callback_query.data.replace("database_", ""))

        db = DBSession.query(PasswordDatabase).filter(PasswordDatabase.user_id == user.id).filter(PasswordDatabase.id == data).first()
        user_data['database_menu'] = {}
        user_data['database_menu']['database'] = db
        user_data['database_menu']['active_item'] = None
        user_data['database_menu']['page'] = 0

        user_data['database_menu']['interface'] = user_data['interface'].edit_text(text=_("Enter your password for {name}").format(name=db.name), reply_markup=self.back_markup(user_data))

        return DatabaseStates.PASSWORD

    def open_database(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']
        text = update.message.text
        try:
            val = validators.UnicodeString()
            value = val.to_python(text)

            hashed_password = hashlib.sha512((value + SECRET).encode('utf-8')).hexdigest()
            if user_data['database_menu']['database'].master_password == hashed_password:
                tpd = TelegramPasswordDatabase(value, user_data['database_menu']['database'].id)
                user_data['database_menu']['tpd'] = tpd
                self.send_message_view(user_data)
                return DatabaseStates.ACTION

            else:
                raise formencode.Invalid("Wrong password", value, None)

        except formencode.Invalid:
            user_data['interface'].edit_reply_markup()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong password, try again"), reply_markup=self.back_markup(user_data))
            return DatabaseStates.PASSWORD

    def delete(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']

        reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton(emojize(":white_heavy_check_mark: ") + _("Yes") + emojize(" :white_heavy_check_mark:"), callback_data='delete_yes')],
                                             [InlineKeyboardButton(emojize(":no_entry:️ ") + _("No") + emojize(" :no_entry:️"), callback_data='delete_no')]])

        user_data['interface'] = user_data['interface'].edit_text(text=_("You really want to delete {name} database?").format(name=user_data['database_menu']['database'].name), reply_markup=reply_markup)
        bot.answer_callback_query(update.callback_query.id)
        return DatabaseStates.DELETE_CONFIRMATION

    def delete_confirmation(self, bot, update, user_data):
        _ = user_data['_']
        user = user_data['user']
        data = update.callback_query.data
        if data == 'delete_yes':
            db = DBSession.query(PasswordDatabase).filter(PasswordDatabase.id == user_data['database_menu']['database'].id).first()
            if not self.remove_from_db(user_data, db):
                return self.conv_fallback(user_data)

            user_data['database_menu']['database'] = None

            user_data['interface'].edit_text(text="Database deleted")

            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=user_data['start_menu']['interface'].text, reply_markup=self.databases_markup(user_data))

            bot.answer_callback_query(update.callback_query.id)
            return ConversationHandler.END

        elif data == 'delete_no':
            self.send_message_view(user_data)
            bot.answer_callback_query(update.callback_query.id)
            return DatabaseStates.ACTION

    def back(self, bot, update, user_data):
        _ = user_data['_']

        if 'database_menu' in user_data and 'tpd' in user_data['database_menu']:
            del user_data['database_menu']['tpd']

        user_data['interface'].edit_text(text=user_data['start_menu']['interface'].text, reply_markup=self.databases_markup(user_data))
        bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def next_page(self, bot, update, user_data):
        _ = user_data['_']
        if 'active_group' in user_data and user_data['active_group']:
            root = user_data['active_group']
        else:
            root = user_data['database_menu']['tpd']

        if len(root.groups) + len(root.entries) <= ITEMS_PER_PAGE:
            bot.answer_callback_query(update.callback_query.id, text=_("There is only one page"))
        else:
            user_data['database_menu']['page'] += 1
            self.send_message_view(user_data)
            bot.answer_callback_query(update.callback_query.id)
        return DatabaseStates.ACTION

    def prev_page(self, bot, update, user_data):
        _ = user_data['_']
        if 'active_group' in user_data and user_data['active_group']:
            root = user_data['active_group']
        else:
            root = user_data['database_menu']['tpd']

        if len(root.groups) + len(root.entries) <= ITEMS_PER_PAGE:
            bot.answer_callback_query(update.callback_query.id, text=_("There is only one page"))
        else:
            user_data['database_menu']['page'] -= 1
            self.send_message_view(user_data)
            bot.answer_callback_query(update.callback_query.id)
        return DatabaseStates.ACTION

    def search(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']
        text = update.message.text.replace('/search ', '')
        entries = user_data['database_menu']['tpd'].search(text)
        if entries:
            fake_group = TelegramPasswordGroup(id=None, title=_("Search results"), notes="")
            fake_group.fake = True
            fake_group.entries = entries
            user_data['active_group'] = fake_group
        else:
            user_data['interface'].delete()
            user_data['interface'] = None
            bot.send_message(chat_id=user.chat_id, text=_("Nothing found."))
        self.send_message_view(user_data)
        return DatabaseStates.ACTION

    def edit_database(self, bot, update, user_data):
        _ = user_data['_']
        user_data['interface'].edit_text(text=_("Enter new name for your database:"))
        return DatabaseStates.EDIT_DATABASE_APPLY

    def edit_database_apply(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        text = update.message.text
        db = DBSession.query(PasswordDatabase).filter(PasswordDatabase.id == user_data['database_menu']['tpd'].id).first()
        db.name = text
        if not self.add_to_db(user_data, db):
            return self.conv_fallback(user_data)

        user_data['database_menu']['tpd'].title = text

        user_data['interface'].delete()
        user_data['interface'] = None
        bot.send_message(chat_id=user.chat_id, text=_("Your database name changed to {name}").format(name=text))
        self.send_message_view(user_data)
        return DatabaseStates.ACTION

    def import_start(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        user_data['interface'].delete()
        user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please send me file of your database in .kdbx format"), reply_markup=self.back_markup(user_data))
        return DatabaseStates.IMPORT_GET_FILE

    def import_get_file(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        file = open(os.path.join(MEDIA_FOLDER, f'{user.chat_id}.kdbx'), 'wb+')

        file_id = update.message.document.file_id
        t_file = bot.get_file(file_id)
        t_file.download(out=file)

        file.close()

        user_data['database_menu']['import_database'] = file.name

        user_data['interface'].edit_reply_markup()
        keyboard = [[InlineKeyboardButton(_('Only password'), callback_data='import_password')],
                    [InlineKeyboardButton(_('Only key-file'), callback_data='import_file')],
                    [InlineKeyboardButton(_('Password and key-file'), callback_data='import_passwordfile')],
                    [InlineKeyboardButton(_('Back'), callback_data='back')]]

        markup = InlineKeyboardMarkup(keyboard, resize_keyboard=True)
        user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("With which combination your kdbx database can be opened?"), reply_markup=markup)
        return DatabaseStates.IMPORT_VARIANT_CHOOSE

    def import_variant_choose(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        bot.answer_callback_query(update.callback_query.id)
        data = update.callback_query.data.replace('import_', '')
        user_data['database_menu']['import_type'] = data

        user_data['interface'].edit_reply_markup()
        if data == 'password':
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please enter your password from imported database"), reply_markup=self.back_markup(user_data))
            return DatabaseStates.IMPORT_PASSWORD
        elif data == 'file':
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please send your file-key which associated with imported database"), reply_markup=self.back_markup(user_data))
            return DatabaseStates.IMPORT_FILE_KEY
        else:
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please enter your password from imported database"), reply_markup=self.back_markup(user_data))
            return DatabaseStates.IMPORT_PASSWORD

    def import_password(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        user_data['interface'].edit_reply_markup()

        text = update.message.text
        try:
            val = validators.UnicodeString()
            value = val.to_python(text)

            user_data['database_menu']['import_password'] = value
            if user_data['database_menu']['import_type'] == 'passwordfile':
                user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please send your file-key which associated with imported database"), reply_markup=self.back_markup(user_data))
                return DatabaseStates.IMPORT_FILE_KEY
            else:
                user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please enter your password from current selected database:"), reply_markup=self.back_markup(user_data))
                return DatabaseStates.IMPORT_REAL_PASSWORD

        except formencode.Invalid:
            user_data['interface'].edit_reply_markup()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong input, try again"), reply_markup=self.back_markup(user_data))
            return DatabaseStates.IMPORT_PASSWORD

    def import_filekey(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        user_data['interface'].edit_reply_markup()

        # we delete it when it will not needed anymore
        file = open(os.path.join(MEDIA_FOLDER, f'{user.chat_id}.key'), 'wb+')

        file_id = update.message.document.file_id
        t_file = bot.get_file(file_id)
        t_file.download(out=file)

        file.close()
        user_data['database_menu']['import_keyfile'] = file.name
        user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please enter your password from current selected database:"), reply_markup=self.back_markup(user_data))
        return DatabaseStates.IMPORT_REAL_PASSWORD

    def import_real_password(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        user_data['interface'].edit_reply_markup()
        text = update.message.text
        try:
            val = validators.UnicodeString()
            value = val.to_python(text)
            hashed_password = hashlib.sha512((value + SECRET).encode('utf-8')).hexdigest()
            database = DBSession.query(PasswordDatabase).filter(PasswordDatabase.id == user_data['database_menu']['tpd'].id).first()

            if hashed_password != database.master_password:
                user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong password from current selected database, try again"))
                user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please enter your password from current selected database:"), reply_markup=self.back_markup(user_data))
                return DatabaseStates.IMPORT_REAL_PASSWORD

            try:
                self.import_process(user_data, value)
            except IOError as e:
                if user_data['database_menu']['import_type'] == 'password':
                    user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong kdbx password, try again"))
                    user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please enter your password from imported database"), reply_markup=self.back_markup(user_data))
                    return DatabaseStates.IMPORT_PASSWORD
                elif user_data['database_menu']['import_type'] == 'file':
                    user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong kdbx keyfile, try again"))
                    user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please send your file-key which associated with imported database"), reply_markup=self.back_markup(user_data))
                    return DatabaseStates.IMPORT_FILE_KEY
                else:
                    user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong password or keyfile, try again."))
                    user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Please enter your password from imported database"), reply_markup=self.back_markup(user_data))
                    return DatabaseStates.IMPORT_PASSWORD

            user_data['interface'] = None

            self.send_message_view(user_data)
            return DatabaseStates.ACTION

        except formencode.Invalid:
            user_data['interface'].edit_reply_markup()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong input, try again"), reply_markup=self.back_markup(user_data))
            return DatabaseStates.IMPORT_REAL_PASSWORD

    def import_process(self, user_data, real_password):
        user = user_data['user']
        _ = user_data['_']

        try:
            password = user_data['database_menu']['import_password'] if 'import_password' in user_data['database_menu'] else None
            file_key = user_data['database_menu']['import_keyfile'] if 'import_keyfile' in user_data['database_menu'] else None

            with PyKeePass(user_data['database_menu']['import_database'], password=password, keyfile=file_key) as kdb:

                key = "{: <32}".format(real_password).encode("utf-8")

                aes_c = AESCipher(key)
                to_db = []
                all_groups_uuids = [uuid_tuple[0] for uuid_tuple in DBSession.query(DatabaseGroup.uuid).filter(DatabaseGroup.database_id == user_data['database_menu']['tpd'].id).all()]
                all_entries_uuids = [uuid_tuple[0] for uuid_tuple in DBSession.query(DatabaseEntry.uuid).filter(DatabaseEntry.database_id == user_data['database_menu']['tpd'].id).all()]
                exists_text_list = []

                def group_parse(parent_group, parent_dg=None):

                    for group in parent_group.subgroups:
                        group_uuid = binascii.hexlify(base64.b64decode(group.uuid)).decode()
                        if group_uuid in all_groups_uuids:
                            exists_text_list.append(_("Group with title \"{title}\" already in database.").format(title=group.name if group.name else _("Unknown title")))
                        else:
                            dg = DatabaseGroup(title=aes_c.encrypt(group.name), notes=aes_c.encrypt(group.notes), uuid=group_uuid, database_id=user_data['database_menu']['tpd'].id, parent_group=parent_dg)
                            group_parse(group, dg)
                            to_db.append(dg)
                    for entry in parent_group.entries:
                        entry_uuid = binascii.hexlify(base64.b64decode(entry.uuid)).decode()
                        if entry_uuid in all_entries_uuids:
                            exists_text_list.append(_("Entry with title \"{title}\" already in database.").format(title=entry.title if entry.title else _("Unknown title")))
                        else:
                            de = DatabaseEntry(uuid=entry_uuid, database_id=user_data['database_menu']['tpd'].id, group=parent_dg)

                            de.title = aes_c.encrypt(entry.title)
                            de.password = aes_c.encrypt(entry.password)
                            de.username = aes_c.encrypt(entry.username)
                            de.notes = aes_c.encrypt(entry.notes)
                            de.url = aes_c.encrypt(entry.url)
                            to_db.append(de)

                group_parse(kdb.root_group, None)

                if not self.add_to_db(user_data, to_db):
                    return self.conv_fallback(user_data)

                if exists_text_list:
                    message_text = _("When importing some messages appears:\n")
                    message_text += '\n'.join(exists_text_list)

                    user_data['interface'] = self.bot.send_message(chat_id=user.chat_id, text=message_text)
                # reload tpd to memory
                user_data['database_menu']['tpd'] = TelegramPasswordDatabase(key, user_data['database_menu']['database'].id)

        except IOError as e:
            logger.error(str(e))
            raise IOError("Master password or key-file wrong")
        except UnicodeDecodeError:
            raise IOError("Critical error, please report to administrator.")
        finally:
            if 'import_keyfile' in user_data['database_menu'] and user_data['database_menu']['import_keyfile']:
                keyfile = user_data['database_menu']['import_keyfile']
                kdbx = user_data['database_menu']['import_database']

                if os.path.exists(keyfile):
                    os.remove(keyfile)

                if os.path.exists(kdbx):
                    os.remove(kdbx)

    def back_to_action(self, bot, update, user_data):
        user_data['interface'].edit_reply_markup()
        user_data['interface'] = None

        self.send_message_view(user_data)

        bot.answer_callback_query(update.callback_query.id)
        return DatabaseStates.ACTION

    def search_quit(self, bot, update, user_data):
        user_data['interface'].delete()
        user_data['interface'] = None

        user_data['active_group'] = None
        self.send_message_view(user_data)

        bot.answer_callback_query(update.callback_query.id)
        return DatabaseStates.ACTION

    def download_ask_password(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']

        bot.answer_callback_query(update.callback_query.id)

        user_data['interface'].delete()
        user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Send me the password with which the exported file will be encrypted"), reply_markup=self.back_markup(user_data))

        return DatabaseStates.DOWNLOAD

    def download(self, bot, update, user_data):
        user = user_data['user']
        _ = user_data['_']
        user_data['interface'].edit_reply_markup()
        text = update.message.text

        export_db_path = os.path.join(MEDIA_FOLDER, f"{user.chat_id}.kdbx")
        sample_db_path = os.path.join(RESOURCES_FOLDER, KDBX_SAMPLE_NAME)
        try:
            val = validators.String()
            value = val.to_python(text)

            with PyKeePass(sample_db_path, password=KDBX_SAMPLE_PASSWORD) as kdb:
                kdb.password = value
                kdb.root_group.name = user_data['database_menu']['tpd'].title
                tpd = user_data['database_menu']['tpd']

                def append_info(parent_telegram_group, parent_kdb_group=None):
                    if parent_kdb_group is None:
                        parent_kdb_group = kdb.root_group

                    for group in parent_telegram_group.groups:
                        created_group = kdb.add_group(parent_kdb_group, group.title.replace("'", "").replace('"', ""), notes=group.notes)
                        append_info(group, created_group)

                    for entry in parent_telegram_group.entries:
                        kdb.add_entry(parent_kdb_group, entry.title.replace("'", "").replace('"', ""), entry.username.replace("'", "").replace('"', ""), entry.password, entry.url, entry.notes, force_creation=True)

                append_info(tpd, None)

                kdb.save(export_db_path)

            del kdb

            with open(export_db_path, 'rb') as file_to_send:
                user_data['interface'] = bot.send_document(chat_id=user.chat_id, document=file_to_send, filename=f"{user_data['database_menu']['tpd'].title}.kdbx", caption=_("It's your exported kdbx database.\nSave it and then press \"Back\" to return to menu."),
                                                           reply_markup=self.back_markup(user_data))

            return DatabaseStates.DOWNLOAD_DONE

        except formencode.Invalid:
            user_data['interface'].edit_reply_markup()
            user_data['interface'] = bot.send_message(chat_id=user.chat_id, text=_("Wrong input, try again"), reply_markup=self.back_markup(user_data))
            return DatabaseStates.DOWNLOAD
        finally:
            if os.path.exists(export_db_path):
                os.remove(export_db_path)

    def download_done(self, bot, update, user_data):
        user_data['interface'].delete()
        user_data['interface'] = None

        self.send_message_view(user_data)

        bot.answer_callback_query(update.callback_query.id)
        return DatabaseStates.ACTION

    def get_handler(self):

        addedit_menu = AddEditMenu()
        entry_menu = EntryMenu()
        group_menu = GroupMenu()
        self.menus.append(addedit_menu)
        self.menus.append(entry_menu)
        self.menus.append(group_menu)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^database_\d+$', pass_user_data=True)],
                                      states={
                                          DatabaseStates.ACTION: [addedit_menu.handler,
                                                                  entry_menu.handler,
                                                                  group_menu.handler,
                                                                  self.resend(DatabaseStates.ACTION),
                                                                  CommandHandler('search', self.search, pass_user_data=True),
                                                                  CommandHandler('import', self.import_start, pass_user_data=True),
                                                                  CallbackQueryHandler(self.download_ask_password, pattern=r'^download$', pass_user_data=True),
                                                                  CallbackQueryHandler(self.next_page, pattern=r'^next_page$', pass_user_data=True),
                                                                  CallbackQueryHandler(self.prev_page, pattern=r'^previous_page$', pass_user_data=True),
                                                                  CallbackQueryHandler(self.edit_database, pattern=r'^edit_database$', pass_user_data=True),
                                                                  CallbackQueryHandler(self.delete, pattern=r'^delete_database$', pass_user_data=True),
                                                                  CallbackQueryHandler(self.search_quit, pattern=r'^back$', pass_user_data=True),
                                                                  CallbackQueryHandler(self.back, pattern=r'^exit$', pass_user_data=True),
                                                                  MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.ACTION))
                                                                  ],
                                          DatabaseStates.PASSWORD: [CallbackQueryHandler(self.back, pattern=r'^back$', pass_user_data=True),
                                                                    MessageHandler(filters.Filters.text, self.open_database, pass_user_data=True),
                                                                    MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.PASSWORD))],
                                          DatabaseStates.DELETE_CONFIRMATION: [CallbackQueryHandler(self.delete_confirmation, pattern=r'^delete_(yes|no)$', pass_user_data=True),
                                                                               MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.DELETE_CONFIRMATION))],
                                          DatabaseStates.EDIT_DATABASE_APPLY: [MessageHandler(filters.Filters.text, self.edit_database_apply, pass_user_data=True),
                                                                               MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.EDIT_DATABASE_APPLY))],
                                          DatabaseStates.IMPORT_GET_FILE: [MessageHandler(filters.Filters.document, self.import_get_file, pass_user_data=True),
                                                                           CallbackQueryHandler(self.back_to_action, pattern=r'^back$', pass_user_data=True),
                                                                           MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.IMPORT_GET_FILE))],
                                          DatabaseStates.IMPORT_VARIANT_CHOOSE: [CallbackQueryHandler(self.import_variant_choose, pattern=r'^import_(password|file|passwordfile)$', pass_user_data=True),
                                                                                 CallbackQueryHandler(self.back_to_action, pattern=r'^back$', pass_user_data=True),
                                                                                 MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.IMPORT_VARIANT_CHOOSE))],
                                          DatabaseStates.IMPORT_FILE_KEY: [MessageHandler(filters.Filters.document, self.import_filekey, pass_user_data=True),
                                                                           CallbackQueryHandler(self.back_to_action, pattern=r'^back$', pass_user_data=True),
                                                                           MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.IMPORT_FILE_KEY))],
                                          DatabaseStates.IMPORT_PASSWORD: [MessageHandler(filters.Filters.text, self.import_password, pass_user_data=True),
                                                                           CallbackQueryHandler(self.back_to_action, pattern=r'^back$', pass_user_data=True),
                                                                           MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.IMPORT_PASSWORD))],
                                          DatabaseStates.IMPORT_REAL_PASSWORD: [MessageHandler(filters.Filters.text, self.import_real_password, pass_user_data=True),
                                                                                CallbackQueryHandler(self.back_to_action, pattern=r'^back$', pass_user_data=True),
                                                                                MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.IMPORT_REAL_PASSWORD))],
                                          DatabaseStates.DOWNLOAD: [MessageHandler(filters.Filters.text, self.download, pass_user_data=True),
                                                                    CallbackQueryHandler(self.back_to_action, pattern=r'^back$', pass_user_data=True),
                                                                    MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.DOWNLOAD))],
                                          DatabaseStates.DOWNLOAD_DONE: [CallbackQueryHandler(self.download_done, pattern=r'^back$', pass_user_data=True),
                                                                         MessageHandler(filters.Filters.all, self.to_state(DatabaseStates.DOWNLOAD_DONE))],
                                      },
                                      fallbacks=[MessageHandler(filters.Filters.all, self.unknown_command(-1), pass_user_data=True)],
                                      allow_reentry=True)

        return handler

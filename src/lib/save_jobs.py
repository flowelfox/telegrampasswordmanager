import copy
import logging
import os
import pickle
import threading
from threading import Event
from time import time

from src.settings import RESOURCES_FOLDER

logger = logging.getLogger(__name__)

JOBS_PICKLE = os.path.join(RESOURCES_FOLDER, 'jobs.pickle')


def load_jobs(jq):
    now = time()

    with open(JOBS_PICKLE, 'rb') as fp:
        while True:
            try:
                next_t, job = pickle.load(fp)
            except EOFError:
                break  # Loaded all job tuples

            # Create threading primitives
            enabled = job._enabled
            removed = job._remove

            job._enabled = Event()
            job._remove = Event()

            if enabled:
                job._enabled.set()

            if removed:
                job._remove.set()

            next_t -= now  # Convert from absolute to relative time

            jq._put(job, next_t)
    logger.info("Jobs loaded")


def save_jobs(jq):
    if jq is not None:
        job_tuples = jq._queue.queue

        with open(JOBS_PICKLE, 'wb') as fp:
            for next_t, job in job_tuples:
                # Back up objects
                job_to_save = copy.copy(job)

                # Replace un-pickleable threading primitives
                job_to_save._job_queue = None  # Will be reset in jq.put
                job_to_save._remove = job._remove.is_set()  # Convert to boolean
                job_to_save._enabled = job._enabled.is_set()  # Convert to boolean

                # Pickle the job
                pickle.dump((next_t, job_to_save), fp)

        logger.debug("Jobs saved")
    else:
        logger.debug("Can't save jobs because job queue is empty")


def save_jobs_job(bot, job):
    save_jobs(job.job_queue)
